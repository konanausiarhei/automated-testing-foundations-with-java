# Automated Testing Foundations with Java

Automated Testing Foundations with Java course from learn.epam.com site.

A merge request is created for each task.

| #    | Merge Request                                                | Branch                                                       |
| ---- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| 1    | [Java. Fundamentals - Main Task](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/1) | [java-fundamentals-main-task](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-fundamentals-main-task) |
| 2    | [Java. Fundamentals - AutoCode. Into](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/2) | [java-fundamentals-autocode-intro](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-fundamentals-autocode-intro) |
| 3    | [Java. Fundamentals - AutoCode. Calculator](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/3) | [java-fundamentals-autocode-calculator](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-fundamentals-autocode-calculator) |
| 4    | [Java. Fundamentals - AutoCode. Matrix processor](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/4) | [java-fundamentals-autocode-matrix-processor](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-fundamentals-autocode-matrix-processor) |
| 5    | [Clean Code - Aircompany](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/5) | [clean-code-aircompany](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/clean-code-aircompany) |
| 6    | [Java. Classes - AutoCode. Domain model](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/6) | [java-classes-autocode-domain-model](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-classes-autocode-domain-model) |
| 7    | [Java. Collections - Autocode. List processor](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/7) | [java-collections-autocode-list-processor](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/java-collections-autocode-list-processor) |
| 8    | [Webdriver - I Can Win](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/8) | [webdriver-i-can-win](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/webdriver-i-can-win) |
| 9    | [Webdriver - Bring It On](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/merge_requests/9) | [webdriver-bring-it-on](https://gitlab.com/konanausiarhei/automated-testing-foundations-with-java/-/tree/webdriver-bring-it-on) |

